# M&S demo project

Requirements (install these):
 - Java 8 (1.8)
 - Maven 3
 - Chrome (any version supported by Selenium)
 
## Running the app

> mvn clean test 


## Inspecting report (with screenshots)

> mvn -DskipTests=true install site
> cd bdd/target/site

open project-reports.html in the browser


## Comments

I've followed scenario from assignment, but I believe it could be improved.
In general, it isn't precise enough. In the example, which shirt we want to put into a basket?What color/size/quantity?
How we know that basket contains only desired item and not something added randomly (apart of the correct item) etc.
Unfortunately due to time constraint, I hadn't a chance to implement those improvements...
