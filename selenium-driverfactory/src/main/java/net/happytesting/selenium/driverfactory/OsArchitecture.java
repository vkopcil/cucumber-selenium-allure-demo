package net.happytesting.selenium.driverfactory;

public enum OsArchitecture {
    x32("amd32"),
    x64("amd64"),
    OTHER("generic");
    
    private static OsArchitecture detected;
    
    private final String[] keys;
    
    private OsArchitecture(String... keys) {
        this.keys = keys;
    }
    
    private boolean match(String archKey) {
        for (String key: keys) {
            if (archKey.indexOf(key) != -1) {
                return true;
            }
        }
        return false;
    }
    
    private static OsArchitecture getOperatingSystemArchitecture(String archKey) {
        for (OsArchitecture osArch: values()) {
            if (osArch.match(archKey)) {
                return osArch;
            }
        }
        return OTHER;
    }
    
    public static OsArchitecture getArchType() {
        if (detected != null) {
            return detected;
        }
        detected = getOperatingSystemArchitecture(System.getProperty("os.arch", OTHER.keys[0]).toLowerCase());
        return detected;
    }
}
