package net.happytesting.selenium.driverfactory.chrome;

import java.io.File;
import java.io.IOException;

import net.happytesting.selenium.driverfactory.DriverFactoryException;
import net.happytesting.selenium.driverfactory.Extractor;
import net.happytesting.selenium.driverfactory.OsArchitecture;
import net.happytesting.selenium.driverfactory.OsType;

import org.openqa.selenium.chrome.ChromeDriver;

public class ChromeDriverFactory {
    private static final String CHROMEDRIVER_LIB_DIR = "chromedriver-2.16";
    private static final String CHROMEDRIVER;
    private static final Extractor extractor;

    static {
        final String folder;
        String driver = "chromedriver";
        switch (OsType.getOsType()) {
        case LINUX:
            switch (OsArchitecture.getArchType()) {
            case x32:
                folder = "linux32";
                break;
            case x64:
                folder = "linux64";
                break;
            default:
                folder = null;
            }
            break;
        case WINDOWS:
            folder = "win32";
            driver = "chromedriver.exe";
            break;
        case MAC_OS:
            folder = "mac32";
            break;
        default:
            folder = null;
        }
        if (folder == null) {
            throw new DriverFactoryException(String.format(
                    "Running on unsupported operating system. %s/%s",
                    System.getProperty("os.name"),
                    System.getProperty("os.arch")));
        }
        CHROMEDRIVER = driver;
        extractor = new Extractor(CHROMEDRIVER_LIB_DIR + "/" + folder);

        try {
            extractor.extractFile(driver);
        } catch (IOException e) {
            throw new DriverFactoryException(e);
        }

    }

    public static ChromeDriver create() {
        System.setProperty("webdriver.chrome.driver", extractor.getLibPath()
                + File.separator + CHROMEDRIVER);
        ChromeDriver driver = new ChromeDriver();
        return driver;
    }
}