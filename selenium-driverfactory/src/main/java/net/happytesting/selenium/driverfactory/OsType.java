package net.happytesting.selenium.driverfactory;

public enum OsType {
    LINUX("nux"),
    WINDOWS("win"),
    MAC_OS("mac", "darwin"),
    OTHER("generic");
    
    private static OsType detected;
    
    private final String[] keys;
    
    private OsType(String... keys) {
        this.keys = keys;
    }
    
    private boolean match(String osKey) {
        for (String key: keys) {
            if (osKey.indexOf(key) != -1) {
                return true;
            }
        }
        return false;
    }
    
    private static OsType getOperatingSystemType(String osKey) {
        for (OsType osType: values()) {
            if (osType.match(osKey)) {
                return osType;
            }
        }
        return OTHER;
    }
    
    public static OsType getOsType() {
        if (detected != null) {
            return detected;
        }
        detected = getOperatingSystemType(System.getProperty("os.name", OTHER.keys[0]).toLowerCase());
        return detected;
    }
}
