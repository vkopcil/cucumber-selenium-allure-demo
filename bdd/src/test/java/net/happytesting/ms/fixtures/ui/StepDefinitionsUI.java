package net.happytesting.ms.fixtures.ui;

import static org.fest.assertions.Assertions.assertThat;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Properties;

import net.happytesting.ms.domain.BasketItem;
import net.happytesting.ms.fixtures.ui.po.BasketPage;
import net.happytesting.ms.fixtures.ui.po.ProductDetailPage;
import net.happytesting.ms.fixtures.ui.po.SearchResultPage;
import net.happytesting.ms.fixtures.ui.po.WelcomePage;
import net.happytesting.selenium.driverfactory.chrome.ChromeDriverFactory;
import net.happytesting.selenium.fragments.PageFactory;

import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

import ru.yandex.qatools.allure.annotations.Attachment;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class StepDefinitionsUI {
  private static final String PROPERTIES_RESOURCE = "test.properties";
  private static final Properties PROPERTIES = new Properties();
  private WebDriver driver;

  static {
    try (InputStream input = StepDefinitionsUI.class.getResourceAsStream("/"
        + PROPERTIES_RESOURCE)) {
      if (input == null) {
        throw new RuntimeException("could not find resource "
            + PROPERTIES_RESOURCE);
      }
      PROPERTIES.load(input);
    } catch (IOException e) {
      throw new RuntimeException("could not load test.properties", e);
    }
  }

  @Before
  public void setup() {
    String SUT = System.getProperty("SUT");
    if (SUT == null || SUT.trim().length() == 0) {
      SUT = PROPERTIES.getProperty("SUT");
    }
    if (SUT == null || SUT.trim().length() == 0) {
      throw new RuntimeException("please provide SUT in test.properties or on the command line (-DSUT=value)");
    }
    driver = ChromeDriverFactory.create();
    driver.get(SUT);
  }

  @After
  public void tearDown() {
    afterScenarioCleanup();
    driver.quit();
  }

  @Given("^I have added a shirt to my bag$")
  public void i_have_added_a_shirt_to_my_bag() throws Throwable {
    final WelcomePage welcomePage = PageFactory.initElements(driver,
        WelcomePage.class);
    welcomePage.doSearch("shirt");
    final SearchResultPage results = PageFactory.initElements(driver,
        SearchResultPage.class);
    results.waitUntilRendered();
    results.openProduct(0);
    final ProductDetailPage productDetail = PageFactory.initElements(driver,
        ProductDetailPage.class);
    productDetail.addToBasket();
  }

  @When("^I view the contents of my bag$")
  public void i_view_the_contents_of_my_bag() throws Throwable {
    final ProductDetailPage productDetail = PageFactory.initElements(driver,
        ProductDetailPage.class);
    productDetail.openBasket();
  }

  @Then("^I can see the contents of the bag include a shirt$")
  public void i_can_see_the_contents_of_the_bag_include_a_shirt()
      throws Throwable {
    final BasketPage basketPage = PageFactory.initElements(driver,
        BasketPage.class);
    basketPage.waitUntilRendered();
    List<BasketItem> items = basketPage.get();
    assertThat(items).as("count of items in the basket").hasSize(1);
    assertThat(items.get(0).getProduct().getName()).as("first product name")
        .containsIgnoringCase("shirt");
  }

  public void afterScenarioCleanup() {
    if (driver != null) {
      captureDom(driver);
      captureScreenshot(driver);
      driver.manage().deleteAllCookies();
    }
  }

  @Attachment(value = "captured screenshot")
  public byte[] captureScreenshot(WebDriver driver) {
    if (!(driver instanceof TakesScreenshot)) {
      return null;
    }
    byte[] capturePage = ((TakesScreenshot) driver)
        .getScreenshotAs(OutputType.BYTES);
    return capturePage;
  }

  @Attachment(value = "captured DOM")
  public String captureDom(WebDriver driver) {
    String capturedDom = driver.getPageSource();
    return capturedDom;
  }
}
