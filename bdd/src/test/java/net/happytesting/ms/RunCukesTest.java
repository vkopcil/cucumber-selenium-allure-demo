package net.happytesting.ms;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(features = { "src/test/resources/net/happytesting/ms/features" }, glue = { "net.happytesting.ms.fixtures.ui" })
public class RunCukesTest {
}
