package net.happytesting.ms.domain;

public class BasketItem {
  private ProductInfo product;
  private long quantity;
  private String currency;
  private double price;

  public final ProductInfo getProduct() {
    return product;
  }

  public final void setProduct(ProductInfo product) {
    this.product = product;
  }

  public final long getQuantity() {
    return quantity;
  }

  public final void setQuantity(long quantity) {
    this.quantity = quantity;
  }

  public final String getCurrency() {
    return currency;
  }

  public final void setCurrency(String currency) {
    this.currency = currency;
  }

  public final double getPrice() {
    return price;
  }

  public final void setPrice(double price) {
    this.price = price;
  }

}
