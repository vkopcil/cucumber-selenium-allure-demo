package net.happytesting.ms.domain;

public class ProductInfo {
  private String brandTitle;
  private String name;
  private String color;
  private String size;
  private String code;

  public final String getBrandTitle() {
    return brandTitle;
  }

  public final void setBrandTitle(String brandTitle) {
    this.brandTitle = brandTitle;
  }

  public final String getName() {
    return name;
  }

  public final void setName(String name) {
    this.name = name;
  }

  public final String getColor() {
    return color;
  }

  public final void setColor(String color) {
    this.color = color;
  }

  public final String getSize() {
    return size;
  }

  public final void setSize(String size) {
    this.size = size;
  }

  public final String getCode() {
    return code;
  }

  public final void setCode(String code) {
    this.code = code;
  }
}
