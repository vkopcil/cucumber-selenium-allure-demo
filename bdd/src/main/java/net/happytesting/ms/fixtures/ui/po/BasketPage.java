package net.happytesting.ms.fixtures.ui.po;

import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import net.happytesting.ms.domain.BasketItem;
import net.happytesting.ms.fixtures.ui.po.fragments.BasketItemFragment;
import net.happytesting.selenium.fragments.PageFactory;
import net.happytesting.selenium.fragments.pagefactory.Driver;

import org.apache.http.protocol.ImmutableHttpProcessor;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class BasketPage {
  @Driver
  private WebDriver driver;

  @FindBy(className = "basket-title-area")
  private WebElement titleArea;

  @FindBy(xpath = "//tbody[@class='basket-list']/tr[position() > 1]")
  private List<WebElement> items;

  public List<BasketItem> get() {
    List<BasketItem> items = new LinkedList<>();
    for (WebElement item : this.items) {
      BasketItemFragment itemFragment = PageFactory.createFragment(driver,
          item, BasketItemFragment.class);
      items.add(itemFragment.get());
    }
    return items;
  }

  private List<WebElement> paymentSummaryElements;
  
  public Map<String, Integer> getPaymentSummaryItems() {
    Map<String, Integer> hm = new LinkedHashMap<>();
    int row = 1;
    for (WebElement element : paymentSummaryElements) {
        String type = element.findElement(By.id("type")).getText();
        String priceStr = element.findElement(By.id("price")).getText();
        try {
          int price = Integer.parseInt(priceStr);
          hm.put(type, price);
        } catch (NumberFormatException e) {
          throw new RuntimeException("couldn't parse price '" + priceStr + "' for the element at row #" + row, e);
        }
        row++;
    }
    return hm;
}

  public void waitUntilRendered() {
    new WebDriverWait(driver, 60).withMessage(
        "waiting for basket to be rendered").until(
        ExpectedConditions.visibilityOf(titleArea));
  }
}
