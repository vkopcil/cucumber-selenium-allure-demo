package net.happytesting.ms.fixtures.ui.po.fragments;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import net.happytesting.ms.domain.ProductInfo;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class ProductInfoFragment {
  private final Pattern COLOR_SIZE_PATTERN = Pattern.compile("(.*), Size (.*)");
  
  @FindBy(className = "product-logo-name")
  private WebElement brandTitle;
  
  @FindBy(xpath = "./h3/a")
  private WebElement name;
  
  @FindBy(xpath = "./p[1]")
  private WebElement colorAndSize;
  
  @FindBy(className = "product-code")
  private WebElement code;
  
  public ProductInfo get() {
    ProductInfo info = new ProductInfo();
    info.setBrandTitle(brandTitle.getText());
    info.setName(name.getText());
    Matcher m = COLOR_SIZE_PATTERN.matcher(colorAndSize.getText());
    if (m.matches()) {
      info.setColor(m.group(1));
      info.setSize(m.group(2));
    } else {
      throw new RuntimeException("could not parse color and size '" + colorAndSize.getText() + "'");
    }
    info.setCode(code.getText().trim());
    return info;
  }
}
