package net.happytesting.ms.fixtures.ui.po;

import java.util.List;

import net.happytesting.selenium.fragments.pagefactory.Driver;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.google.common.base.Predicate;

public class ProductDetailPage extends AbstractClientPage {
  @Driver
  private WebDriver driver;

  @FindBy(xpath = "//input[@type='radio' and @name='colour']/..")
  private List<WebElement> colorSelector;

  @FindBy(xpath = "//input[@type='radio' and @name='size']/..")
  private List<WebElement> sizeSelector;

  @FindBy(xpath = "//input[@type='submit' and starts-with(@class, 'basket')]")
  private WebElement addButton;

  public void addToBasket() {
    if (colorSelector.size() > 1) {
      colorSelector.get(0).click(); // select first available color
    }
    if (sizeSelector.size() > 1) {
      sizeSelector.get(0).click(); // select first available size
    }
    new WebDriverWait(driver, 60).withMessage(
        "waiting for add button to be active").until(
        new Predicate<WebDriver>() {

          @Override
          public boolean apply(WebDriver driver) {
            return addButton.getAttribute("disabled") == null;
          }
        });
    // otherwise the add button is overlaid by the action tool bar
    ((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView(true);", addButton);
    addButton.click();
    new WebDriverWait(driver, 60).withMessage("waiting while adding item")
        .until(new Predicate<WebDriver>() {

          @Override
          public boolean apply(WebDriver driver) {
            return !addButton.getAttribute("value").contentEquals("adding");
          }
        });
  }
}
