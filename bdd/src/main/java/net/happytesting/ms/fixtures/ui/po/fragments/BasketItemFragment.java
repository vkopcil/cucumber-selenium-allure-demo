package net.happytesting.ms.fixtures.ui.po.fragments;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import net.happytesting.ms.domain.BasketItem;
import net.happytesting.selenium.fragments.PageFactory;
import net.happytesting.selenium.fragments.pagefactory.Driver;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class BasketItemFragment {
  private final static Pattern CURRENCY_PRICE_PATTERN = Pattern.compile("([^\\d]*)(\\d+\\.\\d+)([^\\d]*)");
   
  @Driver
  private WebDriver driver;
  
  @FindBy(xpath = ".//div[@class='product-info-wrap']")
  private WebElement productInfo;
  
  @FindBy(className = "price")
  private WebElement currencyAndPrice;
  
  @FindBy(name = "quantity")
  private WebElement quantity;
  
  public BasketItem get() {
    ProductInfoFragment productInfoFragment = PageFactory.createFragment(driver, productInfo, ProductInfoFragment.class);
    BasketItem item = new BasketItem();
    item.setProduct(productInfoFragment.get());
    String currency = currencyAndPrice.getText().trim();
    Matcher m = CURRENCY_PRICE_PATTERN.matcher(currency);
    if (m.matches()) {
      item.setCurrency(m.group(1).length() > 0? m.group(1): m.group(3));
      item.setPrice(Double.parseDouble(m.group(2)));
    } else {
      throw new RuntimeException("could not parse currency and price '" + currency + "'");
    }
    item.setQuantity(Long.parseLong(quantity.getAttribute("value")));
    return item;
  }
}
