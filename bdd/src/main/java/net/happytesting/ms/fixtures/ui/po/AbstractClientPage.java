package net.happytesting.ms.fixtures.ui.po;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public abstract class AbstractClientPage {
  @FindBy(name = "searchTerm")
  private WebElement searchField;
  
  @FindBy(id = "goButton")
  private WebElement goButton;
  
  @FindBy(xpath = "//ul[@class='basket']//a[@title='View basket']")
  private WebElement basket;
  
  public void doSearch(String forWhat) {
    searchField.sendKeys(forWhat);
    goButton.click();
  }
  
  public void openBasket() {
    basket.click();
  }
}
