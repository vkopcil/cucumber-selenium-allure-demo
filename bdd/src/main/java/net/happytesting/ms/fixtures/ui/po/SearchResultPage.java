package net.happytesting.ms.fixtures.ui.po;

import java.util.List;

import net.happytesting.selenium.fragments.pagefactory.Driver;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class SearchResultPage extends AbstractClientPage {
  @Driver
  private WebDriver driver;

  @FindBy(className = "search-results")
  private WebElement criteria;

  @FindBy(xpath = "//li[@itemtype]")
  private List<WebElement> results;

  public void openProduct(int index) {
    results.get(index).click();
  }

  public void waitUntilRendered() {
    new WebDriverWait(driver, 60).withMessage(
        "waiting for search results to be rendered").until(
        ExpectedConditions.visibilityOf(criteria));
  }

  // TODO: for real tests we would need to implement get method, supporting
  // iterator
}
